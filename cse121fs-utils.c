/*
 * CSE 121 Project 1: A Simple File System.
 *
 * Utility functions.
 *
 * Copyright (C) 2010 Kenyon Ralph <kralph@ucsd.edu>
 */

#include "cse121fs.h"

#include <errno.h>
#include <error.h>
#include <limits.h>
#include <stdbool.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

void
blockseek(FILE *disk, uint32_t blocknum)
{
        errno = 0;
        if (fseeko(disk, (off_t)(blocknum * BLOCKSIZE), SEEK_SET) != 0)
        {
                error(EXIT_FAILURE, errno, "blockseek");
        }
}

size_t
blockread(FILE *disk, uint32_t blocknum, unsigned char *buf)
{
        size_t retval;

        blockseek(disk, blocknum);

        errno = 0;
        if ((retval = fread(buf, sizeof(unsigned char), BLOCKSIZE, disk)) !=
            BLOCKSIZE * sizeof(unsigned char))
        {
                error(EXIT_FAILURE, errno, "blockread fread");
        }

        return retval;
}

size_t
blockwrite(FILE *disk, uint32_t blocknum, const unsigned char *buf)
{
        size_t retval;

        blockseek(disk, blocknum);

        errno = 0;
        if ((retval = fwrite(buf, sizeof(unsigned char), BLOCKSIZE, disk)) !=
            BLOCKSIZE * sizeof(unsigned char))
        {
                error(EXIT_FAILURE, errno, "blockwrite fwrite");
        }

        return retval;
}

struct fs
read_superblock(FILE *disk)
{
        unsigned char block[BLOCKSIZE];
        struct fs fs;

        blockread(disk, 0, block);
        memcpy(&fs, block, sizeof(struct fs));

        return fs;
}

void
print_superblock(FILE *disk, struct fs fs)
{
        fprintf(disk,
                "fs: magic=%s, nblocks=%u, ninodes=%u, nfree_inodes=%u, clean=%#x\n",
                (char *)&fs.magic, fs.nblocks, fs.ninodes, fs.nfree_inodes, fs.clean);
}

size_t
write_superblock(FILE *disk, struct fs fs)
{
        unsigned char block[BLOCKSIZE];

        memset(mempcpy(&block, &fs, sizeof(struct fs)), 0,
               sizeof(block) - sizeof(struct fs));

        return blockwrite(disk, 0, block);
}

uint64_t
inode_byte_offset(uint32_t n)
{
        return BLOCKSIZE + (n - 1) * sizeof(struct inode);
}

uint32_t
inode_block(uint32_t n)
{
        return 1 + sizeof(struct inode) * (n - 1) / BLOCKSIZE;
}

struct inode
new_inode(uint32_t mode, uint32_t size, uint32_t nblocks, uint32_t nrefs)
{
        struct inode newinode;

        newinode.mode = mode;
        newinode.size = size;
        newinode.nblocks = nblocks;
        newinode.nrefs = nrefs;

        for (unsigned int i = 0; i < NDADDR; ++i)
        {
                newinode.db[i] = 0;
        }

        return newinode;
}

struct inode
read_inode(FILE *disk, uint32_t n)
{
        unsigned char block[BLOCKSIZE];
        struct inode inode;

        blockread(disk, inode_block(n), block);
        memcpy(&inode, block + inode_byte_offset(n) - BLOCKSIZE *
               inode_block(n), sizeof(struct inode));

        return inode;
}

uint32_t
next_free_inode(FILE *disk)
{
        struct inode inode;

        for (uint32_t i = 1; i < read_superblock(disk).ninodes; ++i)
        {
                inode = read_inode(disk, i);

                if (inode.nrefs == 0)
                {
                        return i;
                }
        }

        return 0;
}

size_t
write_inode(FILE *disk, struct inode inode, uint32_t n)
{
        unsigned char block[BLOCKSIZE];
        struct fs fs = read_superblock(disk);

        fs.nfree_inodes--;

        blockread(disk, inode_block(n), block);
        memset(block + inode_byte_offset(n) - BLOCKSIZE * inode_block(n), 0,
               sizeof(struct inode));
        memcpy(block + inode_byte_offset(n) - BLOCKSIZE * inode_block(n),
               &inode, sizeof(struct inode));

        for (unsigned int i = 0; i < NDADDR; ++i)
        {
                if (inode.db[i] != 0)
                {
                        mark_block_used(disk, inode.db[i]);
                }
        }

        write_superblock(disk, fs);

        return blockwrite(disk, inode_block(n), block);
}

int64_t
inode_path(FILE *disk, const char *path)
{
        unsigned int i;
        bool match;
        char *wr_path = strdupa(path);
        char *cp = strtok(wr_path, PATH_SEPARATOR_STRING);
        struct inode inode;
        struct dirblock dirblock;
        uint32_t inode_num = ROOTDIR_INODE_NUMBER;

        if (cp == NULL)
        {
                return ROOTDIR_INODE_NUMBER;
        }

        inode = read_inode(disk, ROOTDIR_INODE_NUMBER);
        dirblock = read_dirblock(disk, inode.db[0]);

        while (cp != NULL)
        {
                match = false;

                for (i = 0; i < DIRENTS_PER_DIRBLOCK; ++i)
                {
                        if (strcmp(cp, dirblock.dirents[i].name) == 0)
                        {
                                inode_num = dirblock.dirents[i].inode;
                                match = true;
                                break;
                        }
                }

                if (!match && dirblock.nextblock != 0)
                {
                        dirblock = read_dirblock(disk, dirblock.nextblock);
                        continue;
                }

                if (!match)
                {
                        return -ENOENT;
                }

                inode = read_inode(disk, inode_num);

                // FIXME this directory traversal probably isn't right.
                if (inode.mode == IM_DIRECTORY)
                {
                        dirblock = read_dirblock(disk, inode.db[0]);
                }

                cp = strtok(NULL, PATH_SEPARATOR_STRING);
        }

        return inode_num;
}

struct dirblock
new_dirblock(uint32_t nextblock)
{
        struct dirblock db;

        for (unsigned int i = 0; i < DIRENTS_PER_DIRBLOCK; ++i)
        {
                db.dirents[i] = new_dirent(0, "");
        }

        db.nextblock = nextblock;

        for (unsigned int i = 0; i < DIRBLOCK_PADDING; ++i)
        {
                db.padding[i] = 0;
        }

        return db;
}

size_t
write_dirblock(FILE *disk, struct dirblock db)
{
        unsigned char block[BLOCKSIZE];

        if (sizeof(db) != BLOCKSIZE)
        {
                error(EXIT_FAILURE, 0,
                      "write_dirblock(): sizeof(db) != BLOCKSIZE");
        }

        return blockwrite(disk, read_inode(disk, db.dirents[0].inode).db[0],
                          memcpy(block, &db, sizeof(db)));
}

struct dirblock
read_dirblock(FILE *disk, uint32_t db)
{
        unsigned char block[BLOCKSIZE];
        struct dirblock dirblock;

        blockread(disk, db, block);
        memcpy(&dirblock, block, sizeof(struct dirblock));

        return dirblock;
}

int64_t
dirblocknum_path(FILE *disk, const char *path)
{
        return read_inode(disk, inode_path(disk, path)).db[0];
}

struct cse121fs_dirent
new_dirent(uint32_t inode, const char *name)
{
        struct cse121fs_dirent de;

        de.inode = inode;
        strncpy(de.name, name, sizeof(de.name));

        return de;
}

uint32_t
first_data_block(FILE *disk)
{
        struct fs fs = read_superblock(disk);
        return 1 + inode_block(fs.ninodes) + fs.nblocks / (BLOCKSIZE *
                                                           CHAR_BIT);
}

unsigned int
which_freemap_block(FILE *disk, uint32_t blocknum)
{
        struct fs fs = read_superblock(disk);

        if (blocknum > fs.nblocks - 1 || blocknum < first_data_block(disk))
        {
                return 0;
        }

        return 1 + inode_block(fs.ninodes) + blocknum / (BLOCKSIZE * CHAR_BIT);
}

uint32_t
first_freemap_block(FILE *disk)
{
        return which_freemap_block(disk, first_data_block(disk));
}

/**
 * Returns the index of blocknum's bit within its freemap block. This
 * is blocknum minus the block number of the first data block in this
 * freemap block.
 */
unsigned int
freemap_bit_index(FILE *disk, uint32_t blocknum)
{
        return blocknum - (((which_freemap_block(disk, blocknum) -
                             first_freemap_block(disk)) * BLOCKSIZE * CHAR_BIT)
                           + first_data_block(disk));
}

/**
 * Marks a block as used or free, depending on the value of argument
 * "used".  If used is true, mark the block used; otherwise, mark the
 * block free. Returns the return value of the blockwrite() call, or
 * returns zero without writing to disk if the given blocknum is out
 * of the valid range for the disk.
 */
size_t
mark_block_used_or_free(FILE *disk, uint32_t blocknum, bool used)
{
        if (blocknum < first_data_block(disk) ||
            blocknum > read_superblock(disk).ninodes - 1)
        {
                return 0;
        }

        unsigned char block[BLOCKSIZE];
        unsigned char byte;

        unsigned int bit_index = freemap_bit_index(disk, blocknum);
        unsigned int byte_index = bit_index / CHAR_BIT;
        unsigned int bitnum = bit_index - byte_index * CHAR_BIT;

        blockread(disk, which_freemap_block(disk, blocknum), block);
        byte = block[byte_index];

        if (used)
        {
                byte |= 1U << bitnum;
        }
        else
        {
                byte &= ~(1U << bitnum);
        }

        block[byte_index] = byte;

        return blockwrite(disk, which_freemap_block(disk, blocknum), block);
}

size_t
mark_block_free(FILE *disk, uint32_t blocknum)
{
        return mark_block_used_or_free(disk, blocknum, false);
}

size_t
mark_block_used(FILE *disk, uint32_t blocknum)
{
        return mark_block_used_or_free(disk, blocknum, true);
}

bool
block_is_free(FILE *disk, uint32_t blocknum)
{
        unsigned char block[BLOCKSIZE];
        unsigned char byte;

        unsigned int bit_index = freemap_bit_index(disk, blocknum);
        unsigned int byte_index = bit_index / CHAR_BIT;
        unsigned int bitnum = bit_index - byte_index * CHAR_BIT;

        blockread(disk, which_freemap_block(disk, blocknum), block);
        byte = block[byte_index];

        return byte & (1U << bitnum) ? false : true;
}

uint32_t
next_free_block(FILE *disk)
{
        unsigned char block[BLOCKSIZE];
        uint32_t freemap_blocks = read_superblock(disk).nblocks /
                (BLOCKSIZE * CHAR_BIT);
        unsigned char freemap[BLOCKSIZE * freemap_blocks];
        unsigned char *fp = freemap;
        uint32_t bitnum = 0;

        for (unsigned int i = 0; i < freemap_blocks; ++i)
        {
                blockread(disk, first_freemap_block(disk) + i, block);
                fp = mempcpy(fp, block, sizeof(block));
        }

        for (unsigned int i = 0; i < sizeof(freemap); ++i)
        {
                unsigned char byte = freemap[i];

                for (unsigned int j = 0; j < CHAR_BIT; ++j)
                {
                        if ((byte & 1U << j) == 0)
                        {
                                return bitnum + first_data_block(disk);
                        }

                        bitnum++;
                }
        }

        return 0;
}
