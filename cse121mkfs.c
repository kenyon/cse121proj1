/*
 * CSE 121 Project 1: A Simple File System.
 *
 * Format the disk image.
 *
 * Nice command for inspecting the disk image:
 * {echo "------- decimal -------"; od --address-radix=d --format=uz disk.img; echo "------- hex -------"; od --address-radix=d --format=xz disk.img; echo "------- ascii -------"; od --address-radix=d --format=az disk.img} | less
 *
 * Copyright (C) 2005-2007 Alex C. Snoeren <snoeren@cs.ucsd.edu>
 * Copyright (C) 2005 Calvin Hubble
 * Copyright (C) 2006 Kiran Tati <ktati@cs.ucsd.edu>
 * Copyright (C) 2010 Kenyon Ralph <kralph@ucsd.edu>
 */

#include "cse121fs.h"

#include <errno.h>
#include <error.h>
#include <limits.h>
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

/**
 * Makes a new file system disk image.
 *
 * @param filename filename of the disk image to be created.
 * @param size size in kibibytes of the disk image to be created.
 */
int
mkfs(const char *filename, const uint32_t size)
{
        FILE *disk;
        struct fs newfs;
        uint32_t rootdir_inode_number;
        struct inode rootdir_inode;
        struct dirblock rootdir_data;

        uint32_t blocks = (uint64_t)size * BYTES_PER_KIBIBYTE / BLOCKSIZE;
        uint64_t size_in_bytes = (uint64_t)size * BYTES_PER_KIBIBYTE;
        uint32_t ninodes = size_in_bytes / INODE_DENSITY;
        uint32_t freemap_blocks = blocks / (BLOCKSIZE * CHAR_BIT);
        uint32_t inode_blocks = (ninodes * sizeof(struct inode) / BLOCKSIZE);

#ifdef __amd64
        printf("Requested disk size: %u kiB (%lu bytes).\n", size,
               (uint64_t)size * BYTES_PER_KIBIBYTE);
        printf("Creating %s, %lu kiB ", filename, size_in_bytes /
               BYTES_PER_KIBIBYTE);
#elif i386
        printf("Requested disk size: %u kiB (%llu bytes).\n", size,
               (uint64_t)size * BYTES_PER_KIBIBYTE);
        printf("Creating %s, %llu kiB ", filename, size_in_bytes /
               BYTES_PER_KIBIBYTE);
#endif // __amd64
        printf("(%.1f MiB),\n", (double)size_in_bytes / (BYTES_PER_KIBIBYTE *
                                                         BYTES_PER_KIBIBYTE));
        printf("with %u raw blocks at %u bytes per block,\n", blocks,
               BLOCKSIZE);
        printf("with %u blocks for %u %zu-byte inodes at a %u-byte density,\n",
               inode_blocks, ninodes, sizeof(struct inode), INODE_DENSITY);
        printf("with %u blocks for the free block map,\n", freemap_blocks);
        printf("resulting in %u usable data blocks...\n",
               blocks - 1 - inode_blocks - freemap_blocks);

        errno = 0;
        disk = fopen(DISKFILE, "w+b");
        if (disk == NULL)
        {
                error(EXIT_FAILURE, errno, "Couldn't open %s for writing",
                      DISKFILE);
        }

        // Zeroize the whole disk image.
        for (uint64_t i = 0; i < size_in_bytes; ++i)
        {
                errno = 0;
                if (putc(0, disk) == EOF)
                {
                        error(EXIT_FAILURE, errno, "putc returned EOF");
                }
        }

        newfs.magic = CSE121FS_MAGIC;
        newfs.nblocks = blocks;
        newfs.ninodes = ninodes;
        newfs.nfree_inodes = ninodes;
        newfs.clean = FS_CLEAN;

        write_superblock(disk, newfs);

        // Create the root directory.
        if ((rootdir_inode_number = next_free_inode(disk)) !=
            ROOTDIR_INODE_NUMBER)
        {
                error(EXIT_FAILURE, 0,
                      "rootdir inode number must be %u, was given %u",
                      ROOTDIR_INODE_NUMBER, rootdir_inode_number);
        }
        rootdir_inode = new_inode(IM_DIRECTORY, sizeof(rootdir_data), 1, 2);
        if ((rootdir_inode.db[0] = next_free_block(disk)) == 0)
        {
                error(EXIT_FAILURE, 0,
                      "error requesting next free data block number");
        }
        write_inode(disk, rootdir_inode, rootdir_inode_number);
        rootdir_data = new_dirblock(0);
        rootdir_data.dirents[0] = new_dirent(rootdir_inode_number, ".");
        rootdir_data.dirents[1] = new_dirent(rootdir_inode_number, "..");
        write_dirblock(disk, rootdir_data);

        // TODO: functionalize the above steps:
        // create_directory(FILE *disk, uint32_t containing_dir_inode) that
        // makes and writes a dirblock. And a separate
        // create_file(FILE *disk, const char *name, uint32_t containing_dir_inode, const char *datablock)
        // that allocates and writes an inode, adds a cse121fs_dirent for the
        // given name, and writes the datablock.
        // So a directory needs to do create_file then create_directory.
        // Rootdir needs special handling within these functions though because
        // its containing dir is itself.

        errno = 0;
        if (fclose(disk))
        {
                error(EXIT_FAILURE, errno, "Couldn't close %s", DISKFILE);
        }

        printf("Done.\n");
        return 0;
}

/**
 * Prints usage information.
 *
 * @param stream stream to send the output to.
 */
void
usage(FILE *stream)
{
        fprintf(stream, "Usage: %s size\n", program_invocation_short_name);
        fprintf(stream, "  where size is the size of the disk file in "
                "kibibytes.\n");
        fprintf(stream, "\n");
        fprintf(stream, "The maximum disk size is %.0f GiB (%.0f kiB).\n",
                (double)UINT32_MAX / 2 / BYTES_PER_GIBIBYTE * BLOCKSIZE,
                (double)UINT32_MAX / 2 / BYTES_PER_KIBIBYTE * BLOCKSIZE);
        fprintf(stream,
                "The disk file will be called %s in the current directory.\n",
                DISKFILE);
}

int
main(int argc, char *argv[])
{
        uint32_t size = 0;

        /*
         * Possible TODOs:
         *
         * - Check for existence of DISKFILE and refuse to overwrite
             it unless given some "force" option. See the x mode for
             fopen (a GNU extension).
           - Add an option to only show what would happen, not actually create
             the disk image.
         */

        if (argc != 2)
        {
                error(0, 0, "Invalid number of arguments.\n");
                usage(stderr);
                return EXIT_FAILURE;
        }

        errno = 0;
        size = strtol(argv[1], NULL, 10);
        if (errno)
        {
                error(EXIT_FAILURE, errno, "Overflow while parsing size");
        }

        if (size <= 0 || size > FS_MAX_SIZE_kiB)
        {
                error(EXIT_FAILURE, 0, "Invalid size, must be 0 < size <= %u.",
                      FS_MAX_SIZE_kiB);
        }

        mkfs(DISKFILE, size);

        return EXIT_SUCCESS;
}
