/*
 * CSE 121 Project 1: A Simple File System.
 *
 * FUSE daemon.
 *
 * Copyright (C) 2006-2007 Alex C. Snoeren <snoeren@cs.ucsd.edu>
 * Copyright (C) 2010 Kenyon Ralph <kralph@ucsd.edu>
 */

#include "cse121fs.h"

#define FUSE_USE_VERSION 26

#include <assert.h>
#include <errno.h>
#include <error.h>
#include <fuse.h>
#include <libgen.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <time.h>
#include <unistd.h>

static FILE *disk;

/**
 * Initialize filesystem.
 *
 * This is also when you will need to check the integrity of your file
 * system.  For example, if a crash occurred during a file create
 * after an inode was allocated but before a directory entry is
 * updated, such an error should be found and fixed in cse121_mount.
 *
 * The return value is passed as the private_data field of struct
 * fuse_context to all file operations and as a parameter to the
 * cse121_unmount() method.
 */
static void *
cse121_mount(struct fuse_conn_info *conn)
{
        (void)conn;

        struct fs fs;

        errno = 0;
        disk = fopen(DISKFILE, "r+b");
        if (disk == NULL)
        {
                error(EXIT_FAILURE, errno, "Couldn't open %s for writing",
                      DISKFILE);
        }

        fs = read_superblock(disk);
        print_superblock(stdout, fs);

        if (fs.clean == FS_DIRTY)
        {
                printf("%s not cleanly unmounted.\n", DISKFILE);
                // TODO: initiate fsck here.
        }

        fs.clean = FS_DIRTY;
        write_superblock(disk, fs);

        return NULL;
}

/**
 * Unmount is responsible for unmounting the file system. In addition
 * to closing the disk image file descriptor, unmount will need to
 * write out any necessary meta data that might be required the next
 * time the file system is mounted. For instance, it might note that
 * the filesystem was cleanly unmounted, speeding up the integrity
 * check the next time the file system is mounted.
 */
static void
cse121_unmount(void *private_data)
{
        (void)private_data;

        struct fs fs = read_superblock(disk);

        fs.clean = FS_CLEAN;
        write_superblock(disk, fs);

        errno = 0;
        if (fclose(disk))
        {
                error(EXIT_FAILURE, errno, "Couldn't close %s", DISKFILE);
        }
}

/**
 * Given an absolute path to a file or directory (e.g., /foo/bar; all
 * paths will start with the root directory, "/"), you need to return
 * the file attributes, similar to the stat system call.
 *
 * You should return 0 on success or -1 on error (e.g., the path
 * doesn't exist).
 */
static int
cse121_getattr(const char *path, struct stat *stbuf)
{
        struct inode inode;
        int64_t inode_num = inode_path(disk, path);

        if (inode_num < 0)
        {
                return inode_num;
        }

        inode = read_inode(disk, inode_num);

        stbuf->st_nlink = inode.nrefs;
        stbuf->st_uid = 5;
        stbuf->st_gid = 500;
        stbuf->st_rdev = 0;
        stbuf->st_atime = time(NULL);
        stbuf->st_mtime = time(NULL);
        stbuf->st_ctime = time(NULL);

        if (inode.mode == IM_DIRECTORY)
        {
                stbuf->st_mode = 0777 | S_IFDIR;
        }
        else if (inode.mode == IM_REGULAR)
        {
                stbuf->st_mode = 0777 | S_IFREG;
        }
        else
        {
                stbuf->st_mode = 0;
        }

        stbuf->st_size = inode.size;
        stbuf->st_blocks = inode.nblocks;
        stbuf->st_blksize = BLOCKSIZE;

        return 0;
}

/**
 * Given an absolute path to a directory (which may or may not end in
 * '/'), cse121_mkdir will create a new directory named dirname in
 * that directory. Ignore the mode parameter as we are not
 * implementing the permissions. The steps your implementation will do
 * to make a new directory will be the following:
 *
 * 1. Resolve the directory path
 *
 * 2. Allocate and initialize a new directory block
 *
 * 3. Update the directory block for 'path' by adding an entry for the
 *    newly created directory.
 *
 * 4. You also need to create appropriate entries for "." and ".." in
 *    the New directory
 *
 * You should return 0 on success or -1 on error (e.g., the path
 * doesn't exist, or dirname already does).
 */
static int
cse121_mkdir(const char *path, mode_t mode)
{
// TODO: use dirname() and basename() to split the path between the containing
// dir and the name of the new dir.

        return 0;
}

/**
 * Read directory.
 *
 * Given an absolute path to a directory, cse121_readdir will return
 * all the files and directories in that directory. The steps you will
 * take will be the following:
 *
 * 1. Resolve the directory entry from its absolute path to get the
 *    address of the directory block that corresponds to "path"
 *
 * 2. For each valid entry in the directory block, copy the file name
 *    into an array (buf) using the function filler. The filler is
 *    already implemeted in fuse and its sample use is shown in
 *    fusexmp.c file and you should pass zero to the offest paramter
 *    of filler function. fuse.h file has some more information about
 *    how to implement this function and another way of implementing
 *    it.
 *
 * You should return 0 on success or -1 on error (e.g., the directory
 * does not exist).
 */
static int
cse121_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
               off_t offset, struct fuse_file_info *fi)
{
        (void)fi;
        (void)offset;

        int64_t dirblocknum = dirblocknum_path(disk, path);
        struct dirblock dirblock;
        struct inode inode;
        struct stat st;

        if (dirblocknum < 0)
        {
                return -1;
        }

        dirblock = read_dirblock(disk, dirblocknum);

        while (true)
        {
                for (unsigned int i = 0; i < DIRENTS_PER_DIRBLOCK; ++i)
                {
                        memset(&st, 0, sizeof(st));

                        if (dirblock.dirents[i].inode != 0)
                        {
                                st.st_ino = dirblock.dirents[i].inode;
                                inode = read_inode(disk,
                                                   dirblock.dirents[i].inode);

                                if (inode.mode == IM_REGULAR)
                                {
                                        st.st_mode = 0777 | S_IFREG;
                                }
                                else if (inode.mode == IM_DIRECTORY)
                                {
                                        st.st_mode = 0777 | S_IFDIR;
                                }
                                else
                                {
                                        st.st_mode = IM_UNKNOWN;
                                }

                                if (filler(buf, dirblock.dirents[i].name, &st,
                                           0))
                                {
                                        error(EXIT_FAILURE, 0,
                                              "FUSE filler buffer full");
                                }
                        }
                }

                if (dirblock.nextblock != 0)
                {
                        dirblock = read_dirblock(disk, dirblock.nextblock);
                }
                else
                {
                        break;
                }
        }

        return 0;
}

/**
 * Given an absolute path to a file (for example /a/b/myFile),
 * cse121_create will create a new file named myFile in the /a/b
 * directory.  Ignore the mode parameter as we are not implementing
 * the permissions. Also ignore the rdev parameter.  The steps your
 * implementation will do to make a new file will be the following:
 * (similar to the cse121_mkdir except the fourth step)
 *
 * 1. Resolve the directory path
 *
 * 2. Allocate and initialize a new directory entry
 *
 * 3. Update the directory entry for 'path' by adding an entry for the
 *    newly created file.
 *
 * You should return 0 on success or -1 on error (e.g., the path
 * doesn't exist, or file name already exists).
 */
static int
cse121_create(const char *path, mode_t mode, dev_t rdev)
{
        (void)mode;
        (void)rdev;

        bool available_slot = false;
        char *wr_path = strdupa(path);
        int64_t dirblocknum;
        struct cse121fs_dirent newdirent;
        struct dirblock dirblock, newdirblock;
        uint32_t newinode_num = next_free_inode(disk),
                path_dir_inode_num = inode_path(disk, dirname(wr_path)), nfb,
                current_block;
        struct inode newinode,
                path_dir_inode = read_inode(disk, path_dir_inode_num);
        unsigned char block[BLOCKSIZE];
        unsigned int i;

        if (newinode_num == 0)
        {
                return -1;
        }

        wr_path = strdupa(path);
        dirblocknum = dirblocknum_path(disk, dirname(wr_path));

        if (dirblocknum < 0)
        {
                return -1;
        }

        current_block = dirblocknum;
        dirblock = read_dirblock(disk, dirblocknum);
        newinode = new_inode(IM_REGULAR, 0, 0, 1);
        wr_path = strdupa(path);
        newdirent = new_dirent(newinode_num, basename(wr_path));

        for (i = 0; i < DIRENTS_PER_DIRBLOCK; ++i)
        {
                if (dirblock.dirents[i].inode == 0)
                {
                        available_slot = true;
                        break;
                }
        }

        if (!available_slot)
        {
                while (dirblock.nextblock != 0)
                {
                        current_block = dirblock.nextblock;
                        dirblock = read_dirblock(disk, dirblock.nextblock);

                        for (i = 0; i < DIRENTS_PER_DIRBLOCK; ++i)
                        {
                                if (dirblock.dirents[i].inode == 0)
                                {
                                        available_slot = true;
                                        break;
                                }
                        }

                        if (available_slot)
                        {
                                break;
                        }
                }

                if (!available_slot)
                {
                        // Still no available slot, so make a new dirblock and
                        // link to it from the last dirblock examined.

                        nfb = next_free_block(disk);

                        if (nfb == 0)
                        {
                                return -ENOSPC;
                        }

                        path_dir_inode.nblocks++;
                        write_inode(disk, path_dir_inode, path_dir_inode_num);

                        dirblock.nextblock = nfb;
                        mark_block_used(disk, nfb);
                        blockwrite(disk, current_block,
                                   memcpy(block, &dirblock, sizeof(block)));

                        newdirblock = new_dirblock(0);
                        newdirblock.dirents[0] = newdirent;
                        write_inode(disk, newinode, newinode_num);
                        blockwrite(disk, nfb,
                                   memcpy(block, &newdirblock, sizeof(block)));

                        return 0;
                }
                else
                {
                        // Found an available slot in one of the chained
                        // dirblocks.

                        dirblock.dirents[i] = newdirent;
                        write_inode(disk, newinode, newinode_num);
                        blockwrite(disk, current_block,
                                   memcpy(block, &dirblock, sizeof(block)));

                        return 0;
                }
        }

        dirblock.dirents[i] = newdirent;
        write_inode(disk, newinode, newinode_num);
        write_dirblock(disk, dirblock);

        return 0;
}

/**
 * The function cse121_read provides the ability to read data from an
 * absolute path 'path,' which should specify an existing file. It
 * will attempt to read 'size' bytes from the specified file on your
 * filesystem into the memory address 'buf'. The return value is the
 * amount of bytes actually read; if the file is smaller than size,
 * cse121_read will simply return the most amount of bytes it could
 * read.
 *
 * On error, cse121_read will return -1.  The actual implementation of
 * cse121_read is dependent on how files are allocated.
 */
static int
cse121_read(const char *path, char *buf, size_t size, off_t offset,
            struct fuse_file_info *fi)
{
        // FIXME same as cse121_write, this needs to handle indirect blocks, if
        // they are added to struct inode.

        (void)fi;

        struct inode inode;
        uint32_t inode_num = inode_path(disk, path);
        unsigned char data[BLOCKSIZE * NDADDR];
        unsigned int blocks_wanted = 1 + size / BLOCKSIZE;

        if (blocks_wanted > NDADDR)
        {
                blocks_wanted = NDADDR;
        }

        if (inode_num <= 0)
        {
                return -1;
        }

        if (offset + size > BLOCKSIZE * NDADDR)
        {
                return -EFAULT;
        }

        inode = read_inode(disk, inode_num);
        memset(data, 0, BLOCKSIZE * NDADDR);

        if (size > inode.size)
        {
                size = inode.size;
        }

        // FIXME same as write: do I really need to read in the whole file?

        for (unsigned int i = 0; i < NDADDR; ++i)
        {
                if (inode.db[i] != 0)
                {
                        blockread(disk, inode.db[i], data + BLOCKSIZE * i);
                }
        }

        // FIXME what if size + offset > inode.size?

        memcpy(buf, data + offset, size);

        // FIXME is this function working? Why does the inode seem to not have
        // a reference to its data block?

        return size;
}

/**
 * The function cse121_write will attempt to write 'size' bytes from
 * memory address 'buf' into a file specified by an absolute 'path'.
 *
 * On error (e.g., the path does not exist) cse121_write will return
 * -1, otherwise cse121_write should return the number of bytes
 * written.
 */
static int
cse121_write(const char *path, const char *buf, size_t size,
             off_t offset, struct fuse_file_info *fi)
{
        (void)fi;

        uint32_t inode_num = inode_path(disk, path);
        struct inode inode;
        unsigned char data[BLOCKSIZE * NDADDR];

        memset(data, 0, BLOCKSIZE * NDADDR);

        if (inode_num <= 0)
        {
                return -1;
        }

        inode = read_inode(disk, inode_num);

        // FIXME this algorithm won't work if I add indirect blocks to inodes.

        if (size + inode.size > BLOCKSIZE * NDADDR)
        {
                return -EFBIG;
        }

        if (offset + size > BLOCKSIZE * NDADDR)
        {
                return -EFAULT;
        }

        for (unsigned int i = 0; i < NDADDR; ++i)
        {
                if (inode.db[i] != 0)
                {
                        blockread(disk, inode.db[i], data + BLOCKSIZE * i);
                }
        }

        memcpy(data + offset, buf, size);

        // FIXME all of the inode's direct blocks are being allocated and
        // marked used for now, even if they aren't really needed for data.
        // Simpler to implement that way, but this should be fixed.

        for (unsigned int i = 0; i < NDADDR; ++i)
        {
                if (inode.db[i] != 0)
                {
                        blockwrite(disk, inode.db[i], data + BLOCKSIZE * i);
                }
                else
                {
                        inode.db[i] = next_free_block(disk);

                        if (inode.db[i] == 0)
                        {
                                return -ENOSPC;
                        }
                        else
                        {
                                blockwrite(disk, inode.db[i], data + BLOCKSIZE
                                           * i);
                                mark_block_used(disk, inode.db[i]);
                        }
                }
        }

        // FIXME probably wrong here, in a hurry.
        inode.size += size;
        write_inode(disk, inode, inode_num);

        return 0;
}

/**
 * This function deletes the last component of the path (e.g., /a/b/c
 * you need to remove the file 'c' from the directory /a/b).
 *
 * On error (e.g., the path does not exist) cse121_delete will return
 * -1, otherwise cse121_delete should return the number of bytes
 * written.
 */
static int
cse121_delete(const char *path)
{
        char *wr_path = strdupa(path);
        int64_t containing_dirblocknum =
                dirblocknum_path(disk, dirname(wr_path));

        if (containing_dirblocknum < 0)
        {
                return -1;
        }

        unsigned char block[BLOCKSIZE];
        struct fs fs;
        unsigned int i = 0;
        bool found = false;
        int64_t inode_num = inode_path(disk, path);

        if (inode_num < 0)
        {
                return inode_num;
        }

        struct inode inode = read_inode(disk, inode_num);
        struct dirblock containing_dirblock =
                read_dirblock(disk, containing_dirblocknum);

        // Find the real containing dirblock, since read_dirblock only returns
        // the first dirblock in a possible chain.

        do {
                for (i = 0; i < DIRENTS_PER_DIRBLOCK; ++i)
                {
                        if (containing_dirblock.dirents[i].inode == inode_num)
                        {
                                found = true;
                                break;
                        }
                }

                if (!found)
                {
                        if (containing_dirblock.nextblock != 0)
                        {
                                containing_dirblocknum =
                                        containing_dirblock.nextblock;
                        }

                        containing_dirblock =
                                read_dirblock(disk,
                                              containing_dirblock.nextblock);
                }
        } while (!found && containing_dirblock.nextblock != 0);

        assert(found);

        containing_dirblock.dirents[i] = new_dirent(0, "");
        blockwrite(disk, containing_dirblocknum,
                   memcpy(block, &containing_dirblock, sizeof(block)));

        inode.mode = IM_UNKNOWN;
        inode.nrefs = 0;
        write_inode(disk, inode, inode_num);

        for (i = 0; i < NDADDR; ++i)
        {
                if (inode.db[i] != 0)
                {
                        mark_block_free(disk, inode.db[i]);
                }
        }

        fs = read_superblock(disk);
        fs.nfree_inodes++;
        write_superblock(disk, fs);

        return 0;
}

/**
 * The function rename will rename a file or directory named by the
 * string 'from' and rename it to the file name specified by 'to'.
 *
 * As usual, return 0 on success, -1 on failure.
 */
static int
cse121_rename(const char *from, const char *to)
{
        return 0;
}

static struct fuse_operations
cse121_operations =
{
        .init    = cse121_mount,
        .destroy = cse121_unmount,
        .getattr = cse121_getattr,
        .mkdir   = cse121_mkdir,
        .readdir = cse121_readdir,
        .mknod   = cse121_create,
        .read    = cse121_read,
        .write   = cse121_write,
        .unlink  = cse121_delete,
        .rename  = cse121_rename,
};

int
main(int argc, char *argv[])
{
        umask(0);
        return fuse_main(argc, argv, &cse121_operations, NULL);
}
